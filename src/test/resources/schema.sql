DROP TABLE IF EXISTS Branch;
CREATE TABLE Branch(
    id_button                 INTEGER auto_increment  PRIMARY KEY,
    address                   varchar(64) NOT NULL,
    lattitude                 DOUBLE NOT NULL,
    longitude                 DOUBLE NOT NULL);