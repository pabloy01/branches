package com.locations.branches

import com.locations.branches.model.Point
import com.locations.branches.service.BranchService
import com.locations.branches.vo.BranchRequest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@Transactional
class BranchesApplicationTests {

	@Autowired
	lateinit var branchService: BranchService

	@Test
	fun contextLoads() {
	}



	@Test
	fun insert_new_branch() {
		val branchRequest = BranchRequest("test1",20.5, 10.5)
		val responseId = branchService.createBranch(branchRequest)
		assert(responseId>0)
	}

	@Test
	fun update__branch() {
		val branchRequest = BranchRequest("test1",20.5, 10.5)
		val responseId = branchService.createBranch(branchRequest)
		val branchModifyRequest = BranchRequest("test2",10.0,10.0)
		branchService.updateBranch(branchModifyRequest,responseId)
		val branchModified = branchService.getBranchById(responseId)
		assert(branchModified.address == branchModifyRequest.address)
	}

	@Test
	fun remove_branch() {
		val branchRequest = BranchRequest("test1",20.5, 10.5)
		val responseId = branchService.createBranch(branchRequest)
		branchService.removeBranch(responseId)
		assert(branchService.findAll().isEmpty())
	}

	@Test
	fun find_nearest() {
		val branchRequest = BranchRequest("test1",20.5, 20.5)
		val branchRequest2 = BranchRequest("test2",50.5, 50.5)
		val branchRequest3 = BranchRequest("test3",100.5, 100.5)
		branchService.createBranch(branchRequest)
		branchService.createBranch(branchRequest2)
		branchService.createBranch(branchRequest3)
		val nearest = branchService.findNearest(Point(45.5, 45.5))
		assert(nearest!!.address == "test2")

	}

}
