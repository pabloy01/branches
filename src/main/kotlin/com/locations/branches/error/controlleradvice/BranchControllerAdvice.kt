package com.locations.branches.error.controlleradvice

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import com.locations.branches.error.exception.BranchAlreadyExistsException
import com.locations.branches.error.model.ErrorField
import org.slf4j.LoggerFactory
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class BranchControllerAdvice : ResponseEntityExceptionHandler() {
    companion object {
        private val log = LoggerFactory.getLogger(BranchControllerAdvice::class.java)
    }

    override fun handleHttpMessageNotReadable(
            ex: HttpMessageNotReadableException, headers: HttpHeaders, status: HttpStatus, request: WebRequest)
            : ResponseEntity<Any> {
        log.error("A Unreadable Message validation error has occurred , {}", ex)
        var errorResponse = ErrorField("", "", "")
        when (ex.cause) {
            is MissingKotlinParameterException -> {
                val missingKotlinParameterException = ex.cause as MissingKotlinParameterException
                errorResponse = ErrorField("1", missingKotlinParameterException.parameter.name.let { it }
                        ?: "", "Falta parametro");
            }
            is JsonParseException -> {
                val jsonParseException = ex.cause as JsonParseException
                errorResponse = ErrorField("2", "body", "Error de formato de mensaje")
            }
            else -> super.handleHttpMessageNotReadable(ex, headers, status, request)
        }
        return ResponseEntity.badRequest().body(listOf(errorResponse));
    }

    @ExceptionHandler(CustomRuntimeException::class)
    fun customBusinessExceptionHandler(e: CustomRuntimeException, request: WebRequest?): ResponseEntity<Any> {
        val errorMap: MutableMap<String, String> = mutableMapOf()
        errorMap["code"] = "409"
        errorMap["message"] = e.localizedMessage

        log.error("A Business Error has occurred, {}", errorMap)
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMap)

    }

    @ExceptionHandler(RuntimeException::class)
    fun customBusinessExceptionHandler(e: RuntimeException, request: WebRequest?): ResponseEntity<Any> {
        val errorMap: MutableMap<String, String> = mutableMapOf()
        errorMap["code"] = "500"
        errorMap["message"] = e.localizedMessage

        log.error("A Business Error has occurred, {}", errorMap)
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMap)
    }
}