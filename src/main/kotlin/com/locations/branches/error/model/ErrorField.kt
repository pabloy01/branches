package com.locations.branches.error.model

data class ErrorField(  val code: String,
                        val field: String,
                        val message: String)