package com.locations.branches.error.exception

import java.lang.RuntimeException

open class CustomRuntimeException(code: Long, description: String) : RuntimeException(description)