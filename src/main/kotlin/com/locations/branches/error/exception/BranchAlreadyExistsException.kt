package com.locations.branches.error.exception

import java.lang.RuntimeException

class BranchAlreadyExistsException : CustomRuntimeException(1L,"Branch already exists")