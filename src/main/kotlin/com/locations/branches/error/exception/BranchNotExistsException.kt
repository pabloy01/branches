package com.locations.branches.error.exception

class BranchNotExistsException : CustomRuntimeException(2L,"Branch does not  exist")