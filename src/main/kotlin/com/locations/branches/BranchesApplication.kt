package com.locations.branches

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BranchesApplication

fun main(args: Array<String>) {
	runApplication<BranchesApplication>(*args)
}
