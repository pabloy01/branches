package com.locations.branches.vo

data class BranchRequest(val address: String,
                         val latitude: Double,
                         val longitude: Double)