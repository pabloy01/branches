package com.locations.branches.vo

data class BranchResponse(val id: Long,
                            val address: String,
                            val latitude: Double,
                            val longitude: Double)
