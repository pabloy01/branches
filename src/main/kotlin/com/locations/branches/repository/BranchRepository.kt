package com.locations.branches.repository

import com.locations.branches.model.Branch
import org.springframework.data.jpa.repository.JpaRepository

interface BranchRepository : JpaRepository<Branch, Long> {
    fun existsByAddress(address: String) : Boolean
}