package com.locations.branches.controller

import com.locations.branches.model.Point
import com.locations.branches.service.BranchService
import com.locations.branches.vo.BranchRequest
import com.locations.branches.vo.BranchResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@RestController

@RequestMapping("/branches")
class BranchController {

    @Autowired
    lateinit var branchService: BranchService

    @PostMapping("/create_branch")
    fun createBranch(@Validated @RequestBody branchRequest: BranchRequest) : Long {
        return branchService.createBranch(branchRequest);
    }

    @PutMapping("/update_branch/{id}")
    fun updateBranch(@Validated @RequestBody branchRequest: BranchRequest, @PathVariable id: Long) {
        branchService.updateBranch(branchRequest,id)
    }

    @DeleteMapping("/delete_branch/{id}")
    fun deleteBranch(@PathVariable id : Long) {
        branchService.removeBranch(id)
    }

    @GetMapping("/get/{id}")
    fun getBranch(@PathVariable id : Long) : BranchResponse {
        return branchService.getBranchById(id)
    }

    @GetMapping("/list")
    fun listAll() : List<BranchResponse> {
        return branchService.findAll()
    }

    @GetMapping("/find_nearest/{latitude}/{longitude}")
    fun findNearest(@PathVariable latitude: Double, @PathVariable longitude: Double ) : BranchResponse? {
        return branchService.findNearest(Point(latitude,longitude))
    }
}