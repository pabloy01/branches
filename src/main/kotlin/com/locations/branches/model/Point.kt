package com.locations.branches.model

data class Point(val latitude: Double, val longitude: Double)