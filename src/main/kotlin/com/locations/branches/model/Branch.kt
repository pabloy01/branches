package com.locations.branches.model

import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "Branch")
data class Branch(@Id
                  @GeneratedValue(strategy = GenerationType.SEQUENCE)
                  var id: Long,
                  var address: String,
                  var lattitude: Double,
                  var longitude: Double)
