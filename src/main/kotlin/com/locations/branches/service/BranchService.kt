package com.locations.branches.service

import com.locations.branches.error.exception.BranchAlreadyExistsException
import com.locations.branches.error.exception.BranchNotExistsException
import com.locations.branches.model.Branch
import com.locations.branches.model.Point
import com.locations.branches.repository.BranchRepository
import com.locations.branches.vo.BranchRequest
import com.locations.branches.vo.BranchResponse
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.sql.Timestamp
import kotlin.math.absoluteValue
import kotlin.math.pow

@Service
class BranchService {

    @Autowired
    private lateinit var branchRepository: BranchRepository

    fun createBranch(branchRequest: BranchRequest) : Long {
        if (branchRepository.existsByAddress(branchRequest.address))
            throw BranchAlreadyExistsException()
        val branch = Branch(0, branchRequest.address, branchRequest.latitude, branchRequest.longitude);
        val savedBranch = branchRepository.save(branch)
        return savedBranch.id
    }

    fun updateBranch(branchRequest: BranchRequest, id: Long) {
        val branch = recover(id)
        BeanUtils.copyProperties(branchRequest, branch);
        branchRepository.save(branch);
    }

    fun removeBranch(id: Long) {
        val branch = recover(id)
        branchRepository.delete(branch);
    }

    fun getBranchById(id : Long) : BranchResponse {
        val branch = recover(id);
        return BranchResponse(branch.id, branch.address, branch.lattitude, branch.longitude);
    }

    fun findAll() : List<BranchResponse> {
        return branchRepository.findAll().map { BranchResponse(it.id, it.address, it.lattitude, it.longitude) }
    }

    private fun recover(id: Long): Branch {
        if (!branchRepository.existsById(id))
            throw BranchNotExistsException()
        val branch = branchRepository.getOne(id)
        return branch
    }

    private fun getDistance(p1: Point, p2: Point) : Double {
        return ((p1.latitude - p2.latitude).pow(2) + (p1.longitude - p1.longitude).pow(2)).pow(0.5)
    }

    fun findNearest(point: Point) : BranchResponse? {
        return this.findAll().minBy { getDistance(point, Point(it.latitude, it.longitude)) }
    }
}