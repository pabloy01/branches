<article align="center"><h1>Branches</h1></article>

Microservicio para el manejo de sucursales


 
## Stack tecnológico 

Los frameworks y librerías que utilizaremos son:

 - [Java 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) - Java
 - [Kotlin](https://kotlinlang.org/) - Kotlin
 - [Gradle](https://gradle.org/) - Build Tool
 - [Spring Boot Framework](https://spring.io/) - Framework
 - [Swagger 2.9.2](https://swagger.io/) - Document
 - [JUnit Tests](https://junit.org/junit5/) - Unit testing
 
 

## Instalación 

### Dependencias 

Asegurate de tener instalado Java 11 y Git.

Se persistió la Base en la consola de H2, para un acceso y corrida rápida.
Se pensó como alternativa crear una base de datos en Postgre, pero,
 para ya tener la solución corriendo, se decidió que se persista en memoria.

Los tests corren también sobre una BD en memoria.



Ejecuta este comando para instalar las dependencias:

```bash
$ gradle build
```

### Running :runner:

Para correr el proyecto:

```bash
$ gradlew bootRun
```

El entorno de desarrollo corre sobre <http://localhost:9090>.


## Quick Reference

### Endpoints 

## Crear nueva sucursal

```
POST http://{path}//branches/create_branch
```

### Request:

```
{
   "address": "La Plata",
   "latitude": 10.1,
   "longitude": 20.1
 }
```



### Response: (retorna el Id creado)
```
1
```

## Modificar sucursal existente

######Se recibe un Id a modificar y los datos. Devuelve error si no existe.
```

PUT http://{path}//branches/update_branch/{id}
```

### Request:

```
{
   "address": "F. Varela",
   "latitude": 30.1,
   "longitude": 40.1
 }
```


### Response: (No retorna nada)
```

```
## Eliminar sucursal

#### Se utiliza como parametro el ID de sucursal a eliminar. Retora un error si no existe.

```
DELETE http://{path}//branches/delete_branch/{id}
```

### Request
```

```

### Response:

```

```
## Recuperar la sucursal más cercana a un punto dado

```
GET http://{path}/branches/find_nearest/{latitude}/{longitude}

```

### Request:

```
```


### Response:
```
{
    "address": "Avellaneda",
    "latitude": 30.0,
    "longitude": 30.0
}
```


## Recuperar una sucursal por ID
#### Retorna un error si dicha sucursal no existe
```
GET http://{path}/branches/get/{id}

```

### Request:

```
```


### Response:
```
{
    "address": "Avellaneda",
    "latitude": 30.0,
    "longitude": 30.0
}
```



## Listar sucursales existentes
#### Retorna listado de sucursales cargadas
```
GET http://{path}/branches/list

```

### Request:

```
```


### Response:
```
[
 {
    "address": "Avellaneda",
    "latitude": 30.0,
    "longitude": 30.0
},
 {
    "address": "Lanus",
    "latitude": 60.0,
    "longitude": 60.0
}
]
```
